// Переменная для индикатора загрузки
var spinner;
// и его настройки
var spinner_options = {
	lines: 11, // The number of lines to draw
	length: 4, // The length of each line
	width: 3, // The line thickness
	radius: 9, // The radius of the inner circle
	corners: 1, // Corner roundness (0..1)
	rotate: 0, // The rotation offset
	color: '#000', // #rgb or #rrggbb
	speed: 1, // Rounds per second
	trail: 60, // Afterglow percentage
	shadow: false, // Whether to render a shadow
	hwaccel: false, // Whether to use hardware acceleration
	className: 'spinner', // The CSS class to assign to the spinner
	zIndex: 2e9, // The z-index (defaults to 2000000000)
	top: 'auto', // Top position relative to parent in px
	left: 'auto' // Left position relative to parent in px
};

// Изменение вида основного тулбара в зависимости от ширины окна
// и изменение высоты таблицы в зависимости от высоты окна
var viewport_tight=false;
function checkViewport() {
	if ($(window).width()<910) {
		if (!viewport_tight)
			$('.ac-toolbar .btn').tooltip('destroy').tooltip({placement:'bottom'}).find('span').hide();
			$('#toolbar-bottom .btn').tooltip('destroy').tooltip({placement:'top'});
		viewport_tight=true;
	} else {
		if (viewport_tight)
			$('.ac-toolbar .btn').tooltip('destroy').find('span').show();
		viewport_tight=false;
	}
	$('#table-wrapper').css('height',($(window).height()-$('#toolbar-top').height()-$('#toolbar-bottom').height()-$('#filterAccordion').height()-$('#navbar').height()-$('#page-title').height()-105)+'px');
}
$(window).load(checkViewport).resize(checkViewport);

$(function(){
	// Чтобы при первой загрузке не показывались стандартные тайтлы
	$('.ac-toolbar .btn').tooltip('destroy');

	// Заранее включить функцию закрытия контейнера .alert при клике на class="close"
	// при подгрузке нового контента перезапускать не нужно
	$('.alert').alert();


	/* Форма входа
	--------------------------------*/
	$('#login-form-submit').click(function(e){
		e.preventDefault();
		spinner = new Spinner(spinner_options).spin($('#login-box-overlay').show().get(0));
		$.ajax({
			type: 'post',
			url: $('#login-form').attr('action'),
			data: $('#login-form').serialize(),
			success: function(data){
				$('#login-box-overlay').hide();
				spinner.stop();
				switch(data) {
					case '0':
						$('#login-box').effect('shake',{distance:10});
						$('#login-form-name').parent().parent().addClass('error');
						$('#login-form-password').parent().parent().addClass('error');
						$('#error-box').append($('<div class="alert alert-error fade in"><button type="button" class="close" data-dismiss="alert">×</button>Пример ошибки: <ul><li>Нужно заполнить любыми данными оба поля Логин и Пароль</li></ul></div>'));
						break;
					case '1':
						$('#login-box').fadeOut(function(){
							$.cookie('just_logged_in',1);
							document.location.href='/client.html';
						});
						break;
				}
			},
			error: function(a,b,c){
				$('#login-box-overlay').hide();
				spinner.stop();
				alert("Запрос не прошел.\n"+b+': '+c);
			}
		});
	});
	
	
	/* Внутренние страницы
	-------------------------------------------*/
	// Если только залогинился, проиграть анимацию
	if ($.cookie('just_logged_in')==1) {
		$('#navbar').effect('slide',{direction:'up'},800);
		$('#main-container').hide();
		window.setTimeout(function(){ $('#main-container').fadeIn(400,checkViewport); }, 400);
		$.cookie('just_logged_in',0);
	}
	
	// Основная таблица
	//$('#table thead i.icon-info-sign').tooltip({placement:'bottom'});
	
	// DataTables
	/*
    */
	var dataTable = $('#table').dataTable({
		'iDisplayLength': 4,
        'bAutoWidth': true,
        'sDom': 't',
        "oLanguage": {
            "sLengthMenu": "Показывать по _MENU_ строчек",
            "sZeroRecords": "Ничего не найдено",
            "sInfo": "_START_-_END_ из _TOTAL_",
            "sInfoEmpty": "Нет данных",
            "sInfoFiltered": "(filtered from _MAX_ total records)"
        }
    });
    $('.table-control-next').click(function(){
		dataTable.fnPageChange('next');
    });
    
    // Изменять размер контейнера таблицы при разворачивании фильтра
	$('#filterBody').on('shown',checkViewport).on('hidden',checkViewport);
	
	//$('<div id="modal" class="modal fade"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><div class="modal-body">asdasd</div></div>').modal({backdrop:false,keyboard:true}).modal('show');
	
	
	/* Кнопки тулбара
	----------------------------------------*/
	$('.ac-add-btn').click(function(e){
		e.preventDefault();
		$('#acModalWide').modal({
			remote: '/content/ajax/addClientForm.html'
		});
	});
	
});